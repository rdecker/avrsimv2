#ifndef _LED_H
#define _LED_H

#include <stdbool.h>
#include <simavr/sim_avr.h>
#include <simavr/sim_irq.h>

enum {
    IRQ_LED_IN = 0,
    IRQ_LED_COUNT
};

typedef struct {
    avr_t *avr;
    avr_irq_t *irq;
    bool highOn;
    bool isOn;
} led_t;

void led_init(led_t *l, avr_t *avr, bool highOn);
void led_connect(led_t *l, avr_irq_t *in);

#endif //_LED_H
