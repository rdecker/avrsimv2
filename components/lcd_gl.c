#include "lcd_gl.h"

#include <stdbool.h>
#include <time.h>
#include "lcd_font_white.h"

static bool initialized = false;
static GLuint font_texture;
static GLvec2f charSize = {5.0f, 7.0f};

GLvec2f lcd_gl_getSize(lcd_t *b) {
    return (GLvec2f) {
        (5 + (float) b->cols * (charSize.x + 1)),
        (5 + (float) b->rows * (charSize.y + 1))
    };
}

void lcd_gl_init() {
    if (initialized) return;
    
    glGenTextures(1, &font_texture);
    glBindTexture(GL_TEXTURE_2D, font_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, 4,
                 lcd_font_white.width,
                 lcd_font_white.height, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 lcd_font_white.pixel_data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    initialized = true;
}


static void glputchar(char c, lcd_gl_colors_t colors, bool cursor) {
    GLint texT = 0;
    GLint texL = (GLint) ((GLfloat) c * charSize.x);
    GLint texB = (GLint) (texT + charSize.y);
    GLint texR = (GLint) (texL + charSize.x);
    struct timespec time;
    timespec_get(&time, TIME_UTC);
    bool drawCursor = cursor && (long) (time.tv_nsec / 500e6) % 2;


    // char bg
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_TEXTURE_2D);
    glColor32(colors.char_bg);
    glBegin(GL_QUADS);
    glVertex3i(0, 0, 0);
    glVertex3i(5, 0, 0);
    glVertex3i(5, 7, 0);
    glVertex3i(0, 7, 0);
    glEnd();
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    if (!drawCursor) {
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, font_texture);

        glMatrixMode(GL_TEXTURE);
        glLoadIdentity();
        glScalef(1.0f / (GLfloat) lcd_font_white.width, 1.0f / (GLfloat) lcd_font_white.height, 1.0f);
        glMatrixMode(GL_MODELVIEW);
    }
    if (colors.shadow) {
        glColor32(colors.shadow);
        glPushMatrix();
        glTranslatef(.2f, .2f, 0);
        glBegin(GL_QUADS);
        if (drawCursor) {
            glVertex3i(0, 0, 0);
            glVertex3i(0, 7, 0);
            glVertex3i(5, 7, 0);
            glVertex3i(5, 0, 0);
        } else {
            glTexCoord2i(texL, texT);
            glVertex3i(0, 0, 0);
            glTexCoord2i(texL, texB);
            glVertex3i(0, 7, 0);
            glTexCoord2i(texR, texB);
            glVertex3i(5, 7, 0);
            glTexCoord2i(texR, texT);
            glVertex3i(5, 0, 0);
        }
        glEnd();
        glPopMatrix();
    }
    glColor32(colors.text);
    glBegin(GL_QUADS);
    if (drawCursor) {
        glVertex3i(0, 0, 0);
        glVertex3i(0, 7, 0);
        glVertex3i(5, 7, 0);
        glVertex3i(5, 0, 0);
    } else {
        glTexCoord2i(texL, texT);
        glVertex3i(0, 0, 0);
        glTexCoord2i(texL, texB);
        glVertex3i(0, 7, 0);
        glTexCoord2i(texR, texB);
        glVertex3i(5, 7, 0);
        glTexCoord2i(texR, texT);
        glVertex3i(5, 0, 0);
    }
    glEnd();
    
}

void lcd_gl_draw(lcd_t *b, lcd_gl_colors_t colors) {
    GLfloat border = 3.0f;
    GLfloat spacing = 1.0f;
    
    GLfloat bgT = 0.0f;
    GLfloat bgB = bgT + (GLfloat) b->rows * charSize.y + (GLfloat) (b->rows - 1) * spacing + 2 * border;
    GLfloat bgL = 0.0f;
    GLfloat bgR = bgL + (GLfloat) b->cols * charSize.x + (GLfloat) (b->cols - 1) * spacing + 2 * border;
    

    glPushMatrix();
    
    glDisable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    
    glPushMatrix();
    glTranslatef(0.6f, 1.1f, 0);
    glColor32(0x00000066);
    glBegin(GL_QUADS);
    glVertex3f(bgL, bgT, 0);
    glVertex3f(bgL, bgB, 0);
    glVertex3f(bgR, bgB, 0);
    glVertex3f(bgR, bgT, 0);
    glEnd();
    glPopMatrix();

    glColor32(colors.bg);
    glBegin(GL_QUADS);
    glVertex3f(bgL, bgT, 0);
    glVertex3f(bgL, bgB, 0);
    glVertex3f(bgR, bgB, 0);
    glVertex3f(bgR, bgT, 0);
    glEnd();
    
    
    glTranslatef(border, border, 0);

    glColor3f(1.0f, 1.0f, 1.0f);
    bool cursorOn = lcd_cursorState(b) != LCD_CURSOR_OFF;
    const uint8_t offset[] = {0, 0x40, 0x20, 0x60};
    for (int y = 0; y < b->rows; y++) {
        glPushMatrix();
        for (int x = 0; x < b->cols; x++) {
            uint16_t pos = offset[y] + x;
            glputchar(b->vram[pos], colors, cursorOn && pos == b->cursor);
            glTranslatef(charSize.x + spacing, 0, 0);
        }
        glPopMatrix();
        glTranslatef(0, charSize.y + spacing, 0);
    }
    
    glPopMatrix();
}
