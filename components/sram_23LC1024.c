#include "sram_23LC1024.h"

#include <stdio.h>

#include "../common/log.h"

#define SRAM_WARN(msg, ...) L_WARNING("SRAM: " msg, ##__VA_ARGS__)

static void sram_23LC1024_resetState(sram_23LC1024_t *l) {
    l->state = SRAM_23LC1024_STATE_INSTR;
    l->addr = 0;
}
static bool sram_23LC1024_incrAddr(sram_23LC1024_t *l) {
    switch (l->mode) {
        case SRAM_23LC1024_MODE_PAGE:
            l->addr = (l->addr & ~SRAM_23LC1024_PAGE_MASK)
                    | (((l->addr & SRAM_23LC1024_PAGE_MASK) + 1) % SRAM_23LC1024_PAGE_MASK);
            return true;
        case SRAM_23LC1024_MODE_SEQUENTIAL:
            l->addr = (l->addr + 1) % SRAM_23LC1024_SIZE;
            return true;
        default:
            SRAM_WARN("unknown mode (0x%02x)", l->mode);
            /* fall through */
        case SRAM_23LC1024_MODE_BYTE:
            return false;
    }
}
static uint8_t sram_23LC1024_onPreWord(size_t wordNumber, void *param) {
    sram_23LC1024_t *l = (sram_23LC1024_t *) param;
    if (wordNumber == 0) sram_23LC1024_resetState(l);
    switch (l->state) {
        case SRAM_23LC1024_STATE_INSTR:
        case SRAM_23LC1024_STATE_ADDR1:
        case SRAM_23LC1024_STATE_ADDR2:
        case SRAM_23LC1024_STATE_ADDR3:
        case SRAM_23LC1024_STATE_IDLE:
            /* noop */
            break;
        case SRAM_23LC1024_STATE_OP:
            switch (l->instr) {
                case SRAM_23LC1024_INSTR_READ: return l->memory[l->addr];
                case SRAM_23LC1024_INSTR_RDMR: return l->mode;
                case SRAM_23LC1024_INSTR_WRITE:
                case SRAM_23LC1024_INSTR_WRMR :
                case SRAM_23LC1024_INSTR_EDIO :
                case SRAM_23LC1024_INSTR_EQIO :
                case SRAM_23LC1024_INSTR_RSTIO:
                    return 0;
                default:
                    SRAM_WARN("invalid instruction in STATE_OP (0x%02x)", l->instr);
                    break;
            }
            break;
    }
    return 0;
}
static void sram_23LC1024_onWord(size_t wordNumber, uint8_t word, void *param) {
    sram_23LC1024_t *l = (sram_23LC1024_t *) param;
    switch (l->state) {
        case SRAM_23LC1024_STATE_INSTR:
            l->instr = word;
//            printf("Received instruction: 0x%02x\n", l->instr);
            switch (l->instr) {
                case SRAM_23LC1024_INSTR_READ :
                case SRAM_23LC1024_INSTR_WRITE:
                    l->state = SRAM_23LC1024_STATE_ADDR1;
                    break;
                case SRAM_23LC1024_INSTR_EDIO :
                case SRAM_23LC1024_INSTR_EQIO :
                case SRAM_23LC1024_INSTR_RSTIO:
                    // TODO maybe useless without bit-banging
                    SRAM_WARN("unsupported instruction (0x%02x)", l->instr);
                    l->state = SRAM_23LC1024_STATE_IDLE;
                    break;
                case SRAM_23LC1024_INSTR_RDMR :
                case SRAM_23LC1024_INSTR_WRMR :
                    l->state = SRAM_23LC1024_STATE_OP;
                    break;
                default:
                    SRAM_WARN("invalid instruction (0x%02x)", l->instr);
                    break;
            }
            break;
        case SRAM_23LC1024_STATE_ADDR1:
            l->addr |= word & 0x01;
            l->state = SRAM_23LC1024_STATE_ADDR2;
            break;
        case SRAM_23LC1024_STATE_ADDR2:
            l->addr <<= 8;
            l->addr |= word;
            l->state = SRAM_23LC1024_STATE_ADDR3;
            break;
        case SRAM_23LC1024_STATE_ADDR3:
            l->addr <<= 8;
            l->addr |= word;
            l->state = SRAM_23LC1024_STATE_OP;
            break;
        case SRAM_23LC1024_STATE_OP:
            switch (l->instr) {
                case SRAM_23LC1024_INSTR_WRITE:
                    l->memory[l->addr] = word;
                    /* fall through */
                case SRAM_23LC1024_INSTR_READ :
                    if (!sram_23LC1024_incrAddr(l)) l->state = SRAM_23LC1024_STATE_IDLE;
                    break;
                case SRAM_23LC1024_INSTR_WRMR :
                    l->mode = word & 0xC0;
                    /* fall through */
                case SRAM_23LC1024_INSTR_RDMR :
                    l->state = SRAM_23LC1024_STATE_IDLE;
                    break;
                default:
                    SRAM_WARN("invalid instruction in STATE_OP (0x%02x)", l->instr);
                    break;
            }
            break;
        case SRAM_23LC1024_STATE_IDLE:
            /* noop */
            SRAM_WARN("WARNING: unexpected word in STATE_IDLE");
            break;
    }
}

static const char *irq_names[IRQ_SRAM_23LC1024_COUNT] = {
        [IRQ_SRAM_23LC1024_CS] = ">sram_23LC1024.cs",
        [IRQ_SRAM_23LC1024_MOSI] = ">sram_23LC1024.mosi",
        [IRQ_SRAM_23LC1024_MISO] = "<sram_23LC1024.miso"
};

void sram_23LC1024_init(sram_23LC1024_t *l, avr_t *avr) {
    l->avr = avr;
    l->irq = avr_alloc_irq(&avr->irq_pool, 0, IRQ_SRAM_23LC1024_COUNT, irq_names);
    spi_slave_t *spi = &l->spi;
    spi_slave_init(spi, avr);
#if SPI_SLAVE_BITBANG
    spi->polarity = 0;
    spi->phase = 0;
#endif
    spi->param = l;
    spi->onPreWord = sram_23LC1024_onPreWord;
    spi->onWord = sram_23LC1024_onWord;

#if SPI_SLAVE_BITBANG
    spi_slave_connect(spi,
                      l->irq + IRQ_SRAM_23LC1024_SCK,
                      l->irq + IRQ_SRAM_23LC1024_SI,
                      l->irq + IRQ_SRAM_23LC1024_SO,
                      l->irq + IRQ_SRAM_23LC1024_CS);
#else
    spi_slave_connect(spi,
                      l->irq + IRQ_SRAM_23LC1024_MISO,
                      l->irq + IRQ_SRAM_23LC1024_MOSI,
                      l->irq + IRQ_SRAM_23LC1024_CS);
#endif
}

#if SPI_SLAVE_BITBANG
void sram_23LC1024_connect_spi(sram_23LC1024_t *l, avr_irq_t *cs, avr_irq_t *so, avr_irq_t *sck, avr_irq_t *si) {
    avr_connect_irq(cs, l->irq + IRQ_SRAM_23LC1024_CS);
    avr_connect_irq(l->irq + IRQ_SRAM_23LC1024_SO, so);
    avr_connect_irq(sck, l->irq + IRQ_SRAM_23LC1024_SCK);
    avr_connect_irq(si, l->irq + IRQ_SRAM_23LC1024_SI);
}
#else
void sram_23LC1024_connect_spi(sram_23LC1024_t *l, avr_irq_t *cs, avr_irq_t *mosi, avr_irq_t *miso) {
    avr_connect_irq(l->irq + IRQ_SRAM_23LC1024_MISO, miso);
    avr_connect_irq(mosi, l->irq + IRQ_SRAM_23LC1024_MOSI);
    avr_connect_irq(cs, l->irq + IRQ_SRAM_23LC1024_CS);
}
#endif
