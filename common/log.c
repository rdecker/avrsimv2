#include "log.h"

int l_level = L_LEVEL_INFO;
bool l_color = false;


char *strallocf(size_t *outLength, const char *format, ...) {
    va_list args;
    va_start(args, format);
    char *str = vstrallocf(outLength, format, args);
    va_end(args);
    return str;
}

char *vstrallocf(size_t *outLength, const char *format, va_list args) {
    va_list args2;
    va_copy(args2, args);
    size_t length = vsnprintf(NULL, 0, format, args2);
    if (outLength) *outLength = length;
    va_end(args2);
    char *str = malloc(length + 1);
    vsprintf(str, format, args);
    return str;
}
