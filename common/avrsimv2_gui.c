#include "avrsimv2_gui.h"

#define GUI_PREFIX "GUI: "

#define GUI_DEBUG(msg, ...) L_DEBUG(GUI_PREFIX msg, ##__VA_ARGS__)
#define GUI_INFO(msg, ...) L_INFO(GUI_PREFIX msg, ##__VA_ARGS__)
#define GUI_WARNING(msg, ...) L_WARNING(GUI_PREFIX msg, ##__VA_ARGS__)
#define GUI_ERROR(msg, ...) L_ERROR(GUI_PREFIX msg, ##__VA_ARGS__)
#define GUI_ERROR_EXIT(s, msg, ...) L_ERROR_EXIT(s, GUI_PREFIX msg, ##__VA_ARGS__)

static void gui_cb_error(int error, const char* description) {
    GUI_ERROR("GL Error (%i): %s", error, description);
}
static void gui_cb_windowClose(GLFWwindow* window) {
    gui_t *v = glfwGetWindowUserPointer(window);
    GUI_INFO("Exiting... (window close)");
    avrsimv2_exit(v->s, EXIT_SUCCESS);
}
static void gui_cb_framebufferSize(GLFWwindow* window, int width, int height) {
    gui_t *v = glfwGetWindowUserPointer(window);
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, 0, height, 0, 10);
    glTranslatef(0, (GLfloat) height, 0);
    GLvec2f totalSize = v->s->board ? v->s->board->gui.getSize(v->s->board) : (GLvec2f) {100, 100};
    GLfloat sx = (float) width / totalSize.x, sy = (float) height / totalSize.y;
    GLfloat scale = (sx < sy ? sx : sy);
    glScalef(scale, -scale, scale);
}
static void gui_cb_key(GLFWwindow* window, int key, int scancode, int action, int mods) {
    gui_t *v = glfwGetWindowUserPointer(window);
    if (action == GLFW_PRESS && !mods) {
        switch (key) {
            case GLFW_KEY_Q:
                GUI_INFO("Exiting... (user input)");
                avrsimv2_exit(v->s, EXIT_SUCCESS);
                return;
            case GLFW_KEY_R: sim_reset(v->s->sim); return;
            case GLFW_KEY_D: GUI_INFO("DISPLAY=%s", getenv("DISPLAY")); break;
            default: break;
        }
    }
    if (v->s->board) v->s->board->gui.onKey(v->s->board, key, scancode, action, mods);
}

static void gui_gl_drawFrame(gui_t *v) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
    if (v->s->board) v->s->board->gui.draw(v->s->board);
    glPopMatrix();
}
static void gui_gl_init(gui_t *v) {
    glShadeModel(GL_SMOOTH);
    glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
    if (v->s->board) v->s->board->gui.init(v->s->board);
}
static void gui_load_font(gui_t *v, GLFont **font, char *resourceFile) {
    GUI_DEBUG("Loading font %s...", resourceFile);
    char *file = avrsimv2_createResourcePath(v->s, resourceFile);
    if (!(*font = glFontLoad(v->ft, file, 40))) GUI_ERROR_EXIT(v->s, "Could not load font \"%s\"", file);
    free(file);
}

void gui_init(gui_t *v) {
    GUI_INFO("Initializing...");
    
    // Init GLFW
    GUI_DEBUG("Initializing GLFW...");
    glfwSetErrorCallback(gui_cb_error);
    GUI_DEBUG("Error callback set");
    if (!glfwInit()) {
        GUI_ERROR_EXIT(v->s, "Could not initialize GLFW");
    }
    GUI_DEBUG("GLFW initialized");

    // Create Window (the Context)
    GUI_DEBUG("Creating window...");
//    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
//    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
    glfwWindowHint(GLFW_FLOATING, v->floating ? GLFW_TRUE : GLFW_FALSE);
    if (!(v->window = glfwCreateWindow(100, 100,
                                       "Press 'q' to quit",
                                       NULL, NULL))) {
        GUI_ERROR_EXIT(v->s, "Could not create window");
    }
    glfwSetWindowUserPointer(v->window, v);
    glfwSetWindowCloseCallback(v->window, gui_cb_windowClose);
    glfwSetFramebufferSizeCallback(v->window, gui_cb_framebufferSize);
    glfwSetKeyCallback(v->window, gui_cb_key);
    glfwMakeContextCurrent(v->window);
    GUI_DEBUG("Window created");

    // Init GLEW
    GUI_DEBUG("Initializing GLEW...");
    if (glewInit() != GLEW_OK) GUI_ERROR_EXIT(v->s, "Could not initialize GLEW");
    glewExperimental = GL_TRUE;
    GUI_DEBUG("GLEW initialized");

    // Init FreeType
    GUI_DEBUG("Initializing FreeType...");
    if (FT_Init_FreeType(&v->ft)) GUI_ERROR_EXIT(v->s, "Could not initialize freetype");
    gui_load_font(v, &v->fonts.monospace, "RobotoMono-Regular.ttf");
    v->fonts.monospace->color = 0xFFFFFFFF;
    GUI_DEBUG("FreeType initialized");
    
    glfwSwapInterval(1);
    GUI_DEBUG("Swap interval set");

    gui_gl_init(v);
    GUI_DEBUG("GL initialized");
    if (v->s->board) {
        GLfloat initialWindowScale = 6;
        GLvec2f totalSize = v->s->board->gui.getSize(v->s->board);
        totalSize.x *= initialWindowScale;
        totalSize.y *= initialWindowScale;
        glfwSetWindowSize(v->window, (int) totalSize.x, (int) totalSize.y);
        GUI_DEBUG("Board window size set");
    }

    GUI_INFO("Initialized (floating:%u)", v->floating);
}
void gui_loop(gui_t *v) {
    double lastTime = 0;
    while (!v->s->sim->isDone) {
        v->framesPerSecond = 1 / ((v->currentFrameTime = glfwGetTime()) - lastTime);
        lastTime = v->currentFrameTime;
        
        gui_gl_drawFrame(v);
        glfwSwapBuffers(v->window);
        glfwPollEvents();
    }
}
void gui_close(gui_t *v) {
    GUI_DEBUG("Closing font...");
    glFontClose(&v->fonts.monospace);
    GUI_DEBUG("Closing FreeType...");
    FT_Done_FreeType(v->ft);
    GUI_DEBUG("Destroying window...");
    glfwDestroyWindow(v->window);
    GUI_DEBUG("Terminating glfw...");
    glfwTerminate();
}


