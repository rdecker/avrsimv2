#ifndef _BOARD_XML_MEMORY_C
#define _BOARD_XML_MEMORY_C

#include "board_xml_common.c"

#include "../components/sram_23LC1024.h"
#include "../components/memvis_gl.h"




typedef struct {
    sram_23LC1024_t sram;
    board_xml_irq_t cs;
    board_xml_irq_t mosi;
    board_xml_irq_t miso;
} board_xml_sram_t;

static BOARD_XML_COMP_INIT(board_xml_sram_comp_init, data, board, avr) {
    board_xml_data_cast(board_xml_sram_t, sram, data);
    sram_23LC1024_init(&sram->sram, avr);
    sram_23LC1024_connect_spi(&sram->sram,
                              board_xml_irq_get(avr, sram->cs),
                              board_xml_irq_get(avr, sram->mosi),
                              board_xml_irq_get(avr, sram->miso));
}


static void board_xml_sram_parse(board_xml_t *board, xmlNode *node, board_xml_comp_t *comp) {
    comp->init = board_xml_sram_comp_init;
    board_xml_sram_t *sram = comp->data = calloc(1, sizeof(board_xml_sram_t)); // free
    sram->cs = board_xml_irq_parse(xmlUtilsFindChild(node, "cs"));
    sram->mosi = board_xml_irq_parse(xmlUtilsFindChild(node, "mosi"));
    sram->miso = board_xml_irq_parse(xmlUtilsFindChild(node, "miso"));
}







typedef struct {
    memvis_gl_t memory;
    sram_23LC1024_t *sram;
} board_xml_memory_layout_t;

static BOARD_XML_LAYOUT_INIT(board_xml_memory_layout_init, data, board) {
    board_xml_data_cast(board_xml_memory_layout_t, memory, data);
    if (memory->sram) {
        memory->memory.mem = memory->sram->memory;
        if (memory->memory.mem_size == 0) memory->memory.mem_size = SRAM_23LC1024_SIZE;
    } else {
        memory->memory.mem = board->s->sim->avr->data;
        if (memory->memory.mem_size == 0) memory->memory.mem_size = board->s->sim->avr->ramend;
    }
    memvis_gl_init(&memory->memory);
}
static BOARD_XML_LAYOUT_ONLAYOUT(board_xml_memory_layout_onLayout, data, board) {
    board_xml_data_cast(board_xml_memory_layout_t, memory, data);
    glLSimpleObject(memvis_gl_draw(&memory->memory), memvis_gl_getSize(&memory->memory));
}

static void board_xml_memory_layout_parse(board_xml_t *board, xmlNode *node, board_xml_layout_t *layout) {
    layout->init = board_xml_memory_layout_init;
    layout->onLayout = board_xml_memory_layout_onLayout;
    board_xml_memory_layout_t *memory = layout->data = calloc(1, sizeof(board_xml_memory_layout_t)); // free
    
    xmlNode *id = xmlUtilsFindAttrVal(node, "id");
    if (id) memory->sram = &board_xml_findCompDataByAttr(board_xml_sram_t, board, id)->sram;
    memory->memory.mem_size = (size_t) board_xml_hexInt_parse(xmlUtilsFindAttrVal(node, "size"), 0);
    memory->memory.cols = (size_t) board_xml_int_parse(xmlUtilsFindAttrVal(node, "cols"), 256);
    memory->memory.cell.width = board_xml_float_parse(xmlUtilsFindAttrVal(node, "cellWidth"), 0.39f);
    memory->memory.cell.height = board_xml_float_parse(xmlUtilsFindAttrVal(node, "cellHeight"), 0.35f);
    
    const char *colors = board_xml_str_parse(xmlUtilsFindAttrVal(node, "colors"), "grad_spectrum");
    if (!strcmp(colors, "grad_bw"))
        memvis_gl_genColors(&memory->memory.colors, MVGL_C_GRAD_BW);
    else if (!strcmp(colors, "grad_spectrum"))
        memvis_gl_genColors(&memory->memory.colors, MVGL_C_GRAD_SPECTRUM);
    
}



#endif // _BOARD_XML_MEMORY_C