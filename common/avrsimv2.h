#ifndef _AVRSIMV2_H
#define _AVRSIMV2_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

typedef struct avrsimv2_args_t__ avrsimv2_args_t;
typedef struct avrsimv2_t__ avrsimv2_t;

#include "compat.h"
#include "sim_wrapper.h"
#include "board_loader.h"
#include "avrsimv2_gui.h"
#include "log.h"


#define L_ERROR_EXIT(s, msg, ...) do { L_ERROR(msg, ##__VA_ARGS__); avrsimv2_exit(s, EXIT_FAILURE); } while (0)


struct avrsimv2_args_t__ {
    int help;
    int verbosity;
    char *firmwareFile;
    char *mmcu;
    unsigned long frequency;
    unsigned long gdbPort;
    char *boardFile;
    size_t boardArgc;
    char **boardArgv;
    char *xml;
    int useGUI;
    int floatingWindow;
    int wslX11Forwarding;
    int noCTCorrection;
};

struct avrsimv2_t__ {
    sim_t *sim;
    board_t *board;
    gui_t *gui;
    char *directory;
#if IS_WIN
    DWORD origConOutputMode, origConInputMode;
#endif
};

avrsimv2_args_t avrsimv2_args_default(void);
void avrsimv2_args_appendBoardArg(avrsimv2_args_t *args, char *arg);
bool avrsimv2_args_parse(avrsimv2_args_t *args, int *argc, char **(*argv));
void avrsimv2_args_free(avrsimv2_args_t *args);

void avrsimv2_setupCLI(avrsimv2_t *s, int *argc, char **(*argv));
void avrsimv2_init(avrsimv2_t *s);
void avrsimv2_main(avrsimv2_t *s);
void avrsimv2_exit(avrsimv2_t *s, int status);

char *avrsimv2_createResourcePath(avrsimv2_t *s, char *resource);


#endif //_AVRSIMV2_H
