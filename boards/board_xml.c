#include "board_impl.h"

#if IS_WIN
#define LIBXML_STATIC 1
#endif

#include <libxml/tree.h>
#include <libxml/xmlschemas.h>

#include "board_xml_common.c"

#include "board_xml_led.c"
#include "board_xml_button.c"
#include "board_xml_lcd.c"
#include "board_xml_memory.c"
#include "board_xml_layout.c"
#include "board_xml_stats.c"




static void board_xml_comp_parse(board_xml_t *b, xmlNode *node, board_xml_comp_t *comp) {
    BXML_DEBUG("Parsing '%s'", node->name);
    
    comp->type = xmlUtilsCopyStr(node->name); // free
    xmlNode *id = xmlUtilsFindAttrVal(node, "id");
    if (id) comp->id = xmlUtilsCopyStr(id->content); // free
    
    if (xmlUtilsEqualsStr(node->name, "led")) board_xml_led_parse(b, node, comp);
    else if (xmlUtilsEqualsStr(node->name, "button")) board_xml_button_parse(b, node, comp);
    else if (xmlUtilsEqualsStr(node->name, "lcd")) board_xml_lcd_parse(b, node, comp);
    else if (xmlUtilsEqualsStr(node->name, "sram")) board_xml_sram_parse(b, node, comp);
    else if (xmlUtilsEqualsStr(node->name, "gui")) /* noop */;
    else BXML_ERROR("Unknown component '%s'", node->name);
}

static void board_xml_layout_parse(board_xml_t *b, xmlNode *node, board_xml_layout_t *layout) {
    BXML_DEBUG("Layout parsing '%s'", node->name);
    
    layout->type = xmlUtilsCopyStr(node->name); // free

    if (xmlUtilsEqualsStr(node->name, "led")) board_xml_led_layout_parse(b, node, layout);
//    else if (xmlUtilsEqualsStr(node->name, "button")) board_xml_button_layout_parse(b, node, layout);
    else if (xmlUtilsEqualsStr(node->name, "lcd")) board_xml_lcd_layout_parse(b, node, layout);
    else if (xmlUtilsEqualsStr(node->name, "memory")) board_xml_memory_layout_parse(b, node, layout);
    else if (xmlUtilsEqualsStr(node->name, "stats")) board_xml_stats_layout_parse(b, node, layout);
    else if (xmlUtilsEqualsStr(node->name, "spacer")) board_xml_spacer_layout_parse(b, node, layout);
    else if (xmlUtilsEqualsStr(node->name, "vertical")) board_xml_vertical_layout_parse(b, node, layout);
    else if (xmlUtilsEqualsStr(node->name, "horizontal")) board_xml_horizontal_layout_parse(b, node, layout);
    else if (xmlUtilsEqualsStr(node->name, "gui")) board_xml_vertical_layout_parse(b, node, layout);
    else BXML_ERROR("Unknown layout '%s'", node->name);
}

static void board_xml_parse(board_xml_t *b, xmlNode *node) {
    xmlNode *gui = xmlUtilsFindChild(node, "gui");
    
    b->compCount = xmlChildElementCount(node) - (gui ? 1 : 0);
    b->comps = calloc(b->compCount, sizeof(board_xml_comp_t));
    size_t index = 0;
    for (node = node->children; node; node = node->next) if (node->type == XML_ELEMENT_NODE)
        board_xml_comp_parse(b, node, &b->comps[index++]);
    
    if (gui) board_xml_layout_parse(b, gui, &b->layout);
}



#define board_xml_get(b, board) board_xml_t *b = (board_xml_t *) (board)->custom

static BOARD_FUNC_AVR_INIT(board_xml_avr_init, board, avr) {
    board_xml_get(b, board);
    for (size_t i = 0; i < b->compCount; i++) board_xml_comp_init(&b->comps[i], board, avr);
}


static BOARD_FUNC_GUI_INIT(board_xml_gui_init, board) {
    board_xml_get(b, board);
    board_xml_layout_init(&b->layout, board);
}
static void board_xml_gui_layout(board_t *board) {
    board_xml_get(b, board);
    glLSetFont(board->s->gui->fonts.monospace);
    glFontSetSizeFactor(glLGetFont(), 1.5f);
    board_xml_layout_onLayout(&b->layout, board);
}
static BOARD_FUNC_GUI_GETSIZE(board_xml_gui_getSize, board) {
    glLBeginMeasure();
    board_xml_gui_layout(board);
    return glLEndMeasure();
}
static BOARD_FUNC_GUI_DRAW(board_xml_gui_draw, board) {
    glLBeginDraw();
    board_xml_gui_layout(board);
    glLEndDraw();
}
static BOARD_FUNC_GUI_ONKEY(board_xml_gui_onKey, board, key, scancode, action, mods) {
    board_xml_get(b, board);
    for (size_t i = 0; i < b->compCount; i++)
        board_xml_comp_onKey(&b->comps[i], board, key, scancode, action, mods);
    board_xml_layout_onKey(&b->layout, board, key, scancode, action, mods);
}

static BOARD_FUNC_CLOSE(board_xml_close, board) {
    board_xml_get(b, board);
    board_xml_layout_free(&b->layout);
    for (size_t i = 0; i < b->compCount; i++) board_xml_comp_free(&b->comps[i]);
}

static void board_xml_onValidityError(void *ctx, const char *format, ...) {
    va_list args;
    va_start(args, format);
    size_t l;
    char *str = vstrallocf(&l, format, args);
    if (str[l - 1] == '\n') str[l - 1] = '\0';
    BXML_ERROR("%s", str);
    free(str);
    va_end(args);
}
static void board_xml_onValidityWarning(void *ctx, const char *format, ...) {
    va_list args;
    va_start(args, format);
    size_t l;
    char *str = vstrallocf(&l, format, args);
    if (str[l - 1] == '\n') str[l - 1] = '\0';
    BXML_WARNING("%s", str);
    free(str);
    va_end(args);
}

static void board_xml_validate(board_xml_t *b, board_t *board, xmlDoc *doc) {
    char *schemaFile = strallocf(NULL, "%s/boards/board.xsd", board->s->directory);
    xmlSchemaParserCtxt *parser = xmlSchemaNewParserCtxt(schemaFile);
    if (!parser) {
        BXML_ERROR_EXIT(board->s, "Could not create schema parser for '%s'", schemaFile);
        goto exit;
    }
    xmlSchema *schema = xmlSchemaParse(parser);
    if (!schema) {
        BXML_ERROR_EXIT(board->s, "Could not parse schema '%s'", schemaFile);
        goto exit;
    }
    xmlSchemaValidCtxt *validator = xmlSchemaNewValidCtxt(schema);
    if (!validator) {
        BXML_ERROR_EXIT(board->s, "Could not create schema validator");
        goto exit;
    }
    
    xmlSchemaSetValidErrors(validator, board_xml_onValidityError, board_xml_onValidityWarning, NULL);
    xmlSchemaValidateDoc(validator, doc);
    
    exit:
    xmlSchemaFreeValidCtxt(validator);
    xmlSchemaFree(schema);
    xmlSchemaFreeParserCtxt(parser);
    free(schemaFile);
}

static void board_xml_load(board_xml_t *b, board_t *board, char *file) {
    LIBXML_TEST_VERSION
    
    xmlDoc *doc = xmlParseFile(file);
    if (!doc) {
        BXML_ERROR("Error while reading file '%s'", file);
        goto exit;
    }
    
    board_xml_validate(b, board, doc);
    board_xml_parse(b, xmlDocGetRootElement(doc));

    exit:
    xmlFreeDoc(doc);
    xmlCleanupParser();
}

BOARD_LOAD_F(board, argc, argv) {
    if (argc < 2) BXML_ERROR_EXIT(board->s, "Required argument missing");
    if (argc > 2) BXML_WARNING("Unused arguments");
    char *file = argv[1];

    board->avr.init = board_xml_avr_init;
    board->gui.init = board_xml_gui_init;
    board->gui.getSize = board_xml_gui_getSize;
    board->gui.draw = board_xml_gui_draw;
    board->gui.onKey = board_xml_gui_onKey;
    board->close = board_xml_close;

    board_xml_load(board->custom = calloc(1, sizeof(board_xml_t)), board, file);
    
    BXML_INFO("\"%s\" loaded", file);
}





