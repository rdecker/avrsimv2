AVRSimV2 - a cross-platform simavr extension for back- and frontend emulation of peripherals
====
This project uses [simavr](https://github.com/buserror/simavr).


Disclaimer
----
This simulator is only meant for quick and easy testing.  
As it is **only a simulator** it might not always behave exactly like the real thing,
especially when it comes to timings (see [Known Issues](#known-issues)).  
Final checks should therefore only be made with a real board (e.g. PC-Pool).


Contributing
----
AVRSimV2 is not finished and has room for expansion. Feel free to open new issues.  
If you want to contribute or have any ideas or suggestions you are welcome to contact me (information [below](#further-questions)).


Features
----
+ Windows support!
+ Easy to use API
+ Extendable board architecture
+ Auto layout system for GUI
+ XML parser for custom boards/GUIs
+ Emulation (+ GUI) of:
  + LCD
  + LEDs
  + External SRAM (SPI)
  + General memory visualization
+ Emulation (currently no GUI) of:
  + Buttons
+ Debugging support
+ UART support ([example](#uart))
+ Everything else that simavr has

Also see [usage](#usage) for a more detailed explanation


Requirements
----
Besides standard libraries this project uses:
* `simavr`
* `elf`
* `opengl`
* `GLEW`
* `glfw` (v3)
* `freetype` (v6)
* `xml2`

For Windows all required libraries (or their ported versions) come packaged with this repository.


Installation
----
The most recent releases should be [here](https://git.rwth-aachen.de/jonas.broeckmann/avrsimv2/-/releases) or in the `releases` directory.  
The installation depends on your environment. If you have trouble to setup AVRSimV2 feel free to contact me (see [below](#further-questions)).

### Unix
Use CMake to compile or use the binaries directly. That's it...  
Be aware that you might have to install [some libraries](#requirements) first.

### Windows
Use CMake to compile or use the executables directly.


Quick Start Guide for Windows
----
1. Unpack the latest [`avrsimv2-win-x.x.x.zip`](https://git.rwth-aachen.de/jonas.broeckmann/avrsimv2/-/releases) to a location of your choice.
2. Open a command line and make sure you can access the `avrsimv2.exe` from it (`cd` into the directory or append the location to your path).
3. Run `avrsimv2.exe` with the necessary arguments (see [Usage](#usage) and [Examples (Windows)](#examples-windows)). Make sure that any relative filepath passed as an argument is accessible from your current directory.


Usage
----
`avrsimv2 [OPTION]... firmware`

Option                      | Description
--------------------------- | ------------------------------------
`firmware                 ` | Path to the .elf file
`-? --help                ` | Get help
`-v --verbose             ` | Increase the verbosity level by one. Can be used multiple times
`-m --mmcu name           ` | Required
&nbsp;                      |   `name`         - Example: "atmega644"
`-f --freq frequency      ` | The Frequency to use
&nbsp;                      |   `frequency`    - Hz. Example: "2000000"
`-b --board board         ` | Load a board to simulate the peripherals with
&nbsp;                      |   `board`        - The .so or .dll file to load
`-a --board-arg argument  ` | Specify an argument for the board. Can be used multiple times
&nbsp;                      |   `argument`     - The argument to pass to the board
`--xml file               ` | Shorthand for loading XML based boards
&nbsp;                      |   `file`         - The .xml file to use as the board
`-x                       ` | Enable visualization using GLFW
`--floating               ` | Hint for the window to be floating (a.k.a. always-on-top). May not work with X11
`--wsl-X11                ` | Helps to set up X11 forwarding on WSL
`--no-ct-correct          ` | Do not try to correct the cycle time of simavr
`-g port                  ` | Enable debugging (GDB)
&nbsp;                      |   `port`         - The port to use for debugging

### Examples (Windows)

* `avrsimv2 -m atmega644 -f 20000000 -b boards/psp_board.dll -x -g 1234 SPOS.elf`   
  This will simulate with a frequency of 20MHz and visualization the atmega644 on the board "boards/psp_board.dll" with GDB on port 1234 and load the firmware "SPOS.elf"
* `avrsimv2 -m atmega644 -f 20000000 -b boards/board_xml.dll -a boards/psp_board.xml -x SPOS.elf`  
  This will simulate with a frequency of 20MHz and visualization the atmega644 on the board "boards/board_xml.dll", pass "boards/psp_board.xml" as an argument to the board and load the firmware "SPOS.elf"
* `avrsimv2 -m atmega644 -f 20000000 --xml boards/psp_board.xml -x SPOS.elf`  
  This is the same as the previous command, but requires "boards/board_xml.dll" in the same directory as the executable.

### Examples

* `avrsimv2 -m atmega644 -f 20000000 -b boards/psp_board.so -x -g 1234 SPOS.elf`   
  This will simulate with a frequency of 20MHz and visualization the atmega644 on the board "boards/psp_board.so" with GDB on port 1234 and load the firmware "SPOS.elf"
* `avrsimv2 -m atmega644 -f 20000000 -b boards/board_xml.so -a boards/psp_board.xml -x SPOS.elf`  
  This will simulate with a frequency of 20MHz and visualization the atmega644 on the board "boards/board_xml.so", pass "boards/psp_board.xml" as an argument to the board and load the firmware "SPOS.elf"
* `avrsimv2 -m atmega644 -f 20000000 --xml boards/psp_board.xml -x SPOS.elf`  
  This is the same as the previous command, but requires "boards/board_xml.so" in the same directory as the executable.


Debugging
----
The `-g port` option starts a GDB Server on the specified port. One can either manually connect to it or use an IDE.  
Most IDEs support GDB Server as a target or have a plug-in that has this ability. Atmel/Microchip Studio has (to my knowledge) neither.

Keep in mind that simulating while debugging can be much slower.


Screenshots
----
![LCD, LEDs and memory visualization](img/AVRSimV2_03.png)
![LCD theme blue](img/AVRSimV2_11.png)
![LCD theme dark white](img/AVRSimV2_12.png)
![LCD theme dark red](img/AVRSimV2_13.png)
![LCD theme dark green](img/AVRSimV2_14.png)


Tips and Tricks
----
### UART
The Universal Asynchronous Receiver/Transmitter (UART) allows us to send or receive information on the microchip.
To use `stdout` on your mcu for debugging you could do something like this:
```c
#include "atmega644constants.h"
#define BAUD 9600UL               // used in <util/setbaud.h>
#include <util/setbaud.h>

static int uart_putchar(char c, FILE *ignored) {
    if (c == '\n') uart_putchar('\r');
    while (!(UCSR0A & (1 << UDRE0)));
    UDR0 = c;
    return 1;
}
static FILE _stdout = FDEV_SETUP_STREAM(&uart_putchar, NULL, _FDEV_SETUP_WRITE);
static void __attribute__((constructor)) uart_init(void) {
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;

#if USE_2X
    UCSR0A |= (1 << U2X0);
#else
    UCSR0A &= ~(1 << U2X0);
#endif

    UCSR0B |= (1 << TXEN0);
    UCSR0C = (1 << UMSEL01) | (1 << UCSZ01) | (1 << UCSZ00);
    stdout = &_stdout;
}
```
Now you can use the `printf`-family and the simulator will print everything to console.


Known Issues
----
* Because simavr is not required to have precise cycle timings,
  delays and timing dependent constructs other than `sleep_mode` may not function as expected.
  AVRSimV2 tries to combat this by delaying simavr cycles accordingly.
  However, this solution is far from perfect, as it can only coarsely adjust the timings.
* Under some circumstances the LCD shows wrong characters.
  This might be related to some simulation math issues and should be caused by simavr.
* Some crashes on Windows might lead to the current CMD instance becoming unusable.
* Unconfirmed segmentation faults on Windows. Reason unknown
* ...


Further Questions
----
Please feel free to open new issues or contact me at any time:

+ Email: [jonas.broeckmann@rwth-aachen.de](mailto:jonas.broeckmann@rwth-aachen.de?subject=[AVRSimV2]%20I%20have%20a%20question)
+ Telegram: RWTH Informatik PSP
