#ifndef _BOARD_XML_LAYOUT_C
#define _BOARD_XML_LAYOUT_C

#include "board_xml_common.c"

#include <stdbool.h>

typedef struct {
    GLfloat size;
} board_xml_spacer_layout_t;

static BOARD_XML_LAYOUT_ONLAYOUT(board_xml_spacer_layout_onLayout, data, board) {
    board_xml_data_cast(board_xml_spacer_layout_t, spacer, data);
    glLSpacer(spacer->size);
}

static void board_xml_spacer_layout_parse(board_xml_t *b, xmlNode *node, board_xml_layout_t *layout) {
    layout->onLayout = board_xml_spacer_layout_onLayout;
    board_xml_spacer_layout_t *spacer = layout->data = calloc(1, sizeof(board_xml_spacer_layout_t)); // free

    spacer->size = board_xml_float_parse(xmlUtilsFindAttrVal(node, "size"), 2.0f);
}



typedef struct {
    bool isHorizontal;
    size_t childCount;
    board_xml_layout_t *children;
} board_xml_linear_layout_t;

static BOARD_XML_LAYOUT_INIT(board_xml_linear_layout_init, data, board) {
    board_xml_data_cast(board_xml_linear_layout_t, linear, data);
    for (size_t i = 0; i < linear->childCount; i++) board_xml_layout_init(&linear->children[i], board);
}
static BOARD_XML_LAYOUT_ONLAYOUT(board_xml_linear_layout_onLayout, data, board) {
    board_xml_data_cast(board_xml_linear_layout_t, linear, data);
    linear->isHorizontal ? glLBeginHorizontal() : glLBeginVertical();
    for (size_t i = 0; i < linear->childCount; i++) board_xml_layout_onLayout(&linear->children[i], board);
    linear->isHorizontal ? glLEndHorizontal() : glLEndVertical();
}
static BOARD_XML_LAYOUT_ONKEY(board_xml_linear_layout_onKey, data, board, key, scancode, action, mods) {
    board_xml_data_cast(board_xml_linear_layout_t, linear, data);
    for (size_t i = 0; i < linear->childCount; i++)
        board_xml_layout_onKey(&linear->children[i], board, key, scancode, action, mods);
}
static BOARD_XML_LAYOUT_FREE(board_xml_linear_layout_free, data) {
    board_xml_data_cast(board_xml_linear_layout_t, linear, data);
    for (size_t i = 0; i < linear->childCount; i++) board_xml_layout_free(&linear->children[i]);
    free(linear->children);
}


static void board_xml_linear_layout_parse(board_xml_t *b, xmlNode *node, board_xml_layout_t *layout, bool isHorizontal) {
    layout->init = board_xml_linear_layout_init;
    layout->onLayout = board_xml_linear_layout_onLayout;
    layout->onKey = board_xml_linear_layout_onKey;
    layout->free = board_xml_linear_layout_free;
    board_xml_linear_layout_t *linear = layout->data = calloc(1, sizeof(board_xml_linear_layout_t)); // free
    
    linear->isHorizontal = isHorizontal;
    linear->childCount = xmlChildElementCount(node);
    board_xml_layout_t *children = linear->children = calloc(linear->childCount, sizeof(board_xml_layout_t));
    size_t index = 0;
    for (node = node->children; node; node = node->next) if (node->type == XML_ELEMENT_NODE)
        board_xml_layout_parse(b, node, &children[index++]);
}
static void board_xml_vertical_layout_parse(board_xml_t *b, xmlNode *node, board_xml_layout_t *layout) {
    board_xml_linear_layout_parse(b, node, layout, false);
}
static void board_xml_horizontal_layout_parse(board_xml_t *b, xmlNode *node, board_xml_layout_t *layout) {
    board_xml_linear_layout_parse(b, node, layout, true);
}




#endif // _BOARD_XML_LAYOUT_C