#include "led_gl.h"

#include "../common/gl_utils.h"

static float radius = 4.0f;

GLvec2f led_gl_getSize(void) {
    GLfloat d = 2.0f * radius;
    return (GLvec2f) {d, d};
}
void led_gl_draw(led_t *b, uint32_t color) {
    glPushMatrix();
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_TEXTURE_2D);

    GLvec2f pos = {radius, radius};
    
    glPushMatrix();
    glTranslatef(.4f, .8f, 0);
    glColor32(b->isOn ? 0x00000022 : 0x00000055);
    glDrawCircle(pos, radius);
    glPopMatrix();


    glColor32(0x000000FF);
    glDrawCircle(pos, radius);

    glColor32((color & 0xFFFFFF00u) | (b->isOn ? 0xFFu : 0x44u));
    glDrawCircle(pos, radius);
    
    glPopMatrix();
}
