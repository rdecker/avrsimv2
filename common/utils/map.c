#include "map.h"


MAP_DECL_HASH(map_hash_char, char) {
    return key;
}
MAP_DECL_HASH(map_hash_wchar, wchar_t) {
    return key;
}
MAP_DECL_HASH(map_hash_uint, unsigned int) {
    return key;
}
MAP_DECL_HASH(map_hash_ulong, unsigned long) {
    return key;
}
MAP_DECL_HASH(map_hash_str, const char *) {
    size_t h = 5381;
    char c;
    while ((c = *key++)) h = ((h << 5) + h) + map_hash_char(c);
    return h;
}


