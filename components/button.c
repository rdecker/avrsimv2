#include "button.h"


static const char *irq_names[IRQ_BUTTON_COUNT] = {
        [IRQ_BUTTON_OUT] = "<button.out"
};



void button_init(button_t *b, avr_t *avr) {
    b->avr = avr;
    b->irq = avr_alloc_irq(&avr->irq_pool, 0, IRQ_BUTTON_COUNT, irq_names);
}
void button_connect(button_t *b, avr_irq_t *out) {
    avr_connect_irq(b->irq + IRQ_BUTTON_OUT, out);
}


static avr_cycle_count_t button_auto_release(avr_t *avr, avr_cycle_count_t when, void *param) {
    button_t *b = (button_t *) param;
    avr_raise_irq(b->irq + IRQ_BUTTON_OUT, 1);
    return 0;
}


void button_release(button_t *b) {
    avr_cycle_timer_cancel(b->avr, button_auto_release, b);
    avr_raise_irq(b->irq + IRQ_BUTTON_OUT, 1);
}


void button_press(button_t *b, uint32_t duration_usec) {
    avr_cycle_timer_cancel(b->avr, button_auto_release, b);
    avr_raise_irq(b->irq + IRQ_BUTTON_OUT, 0);
    if (duration_usec) avr_cycle_timer_register_usec(b->avr, duration_usec, button_auto_release, b);
}

void button_hold(button_t *b) {
    button_press(b, 0);
}

