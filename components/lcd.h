#ifndef _LCD_H
#define _LCD_H

// Based on HD44780

#include <stdio.h>
#include <stdbool.h>
#include <simavr/sim_avr.h>
#include <simavr/sim_irq.h>


extern bool lcd_doPrint;

enum {
    IRQ_LCD_ALL = 0,  
    IRQ_LCD_RS,
    IRQ_LCD_RW,
    IRQ_LCD_E,
    // bidirectional
    IRQ_LCD_D0, IRQ_LCD_D1, IRQ_LCD_D2, IRQ_LCD_D3,
    IRQ_LCD_D4, IRQ_LCD_D5, IRQ_LCD_D6, IRQ_LCD_D7,
    // count
    IRQ_LCD_INPUT_COUNT,

    // IRQs useful for VCD traces
    IRQ_LCD_BUSY = IRQ_LCD_INPUT_COUNT,
    IRQ_LCD_ADDR,
    IRQ_LCD_DATA_IN,
    IRQ_LCD_DATA_OUT,
    IRQ_LCD_COUNT
};

typedef enum {
    LCD_CURSOR_OFF = 0,
    LCD_CURSOR_ON,
    LCD_CURSOR_BLINK,
} lcd_cursor_state;

#define lcd_ddram_size 80u
#define lcd_cgram_size 64u

typedef struct {
    avr_t *avr;
    avr_irq_t *irq;
    uint8_t cols, rows;

    uint16_t cursor;
    uint8_t vram[lcd_ddram_size + lcd_cgram_size];

    uint16_t pinstate;
    uint8_t datapins;
    uint8_t readpins;

    uint16_t flags;
} lcd_t;

void lcd_init(lcd_t *b, avr_t *avr, int width, int height);
void lcd_connect4(lcd_t *b,
                  avr_irq_t *D4, avr_irq_t *D5, avr_irq_t *D6, avr_irq_t *D7,
                  avr_irq_t *RS, avr_irq_t *E, avr_irq_t *RW);
void lcd_connect8(lcd_t *b,
                  avr_irq_t *D0, avr_irq_t *D1, avr_irq_t *D2, avr_irq_t *D3,
                  avr_irq_t *D4, avr_irq_t *D5, avr_irq_t *D6, avr_irq_t *D7,
                  avr_irq_t *RS, avr_irq_t *E, avr_irq_t *RW);
void lcd_print(lcd_t *b, FILE *stream);
lcd_cursor_state lcd_cursorState(lcd_t *b);

#endif 
