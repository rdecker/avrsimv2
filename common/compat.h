#ifndef _COMPAT_H
#define _COMPAT_H


#ifdef _MSC_VER
#define IS_WIN 1
#else
#define IS_WIN 0
#endif

#if IS_WIN
#define PSEP "\\"
#else
#define PSEP "/"
#endif


#if IS_WIN
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif


#if IS_WIN
#define popen _popen
#define pclose _pclose
#define putenv _putenv
#endif







#endif //_COMPAT_H
