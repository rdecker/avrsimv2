#include "gl_utils.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "log.h"


// region GL

#define GL_ERROR(msg, ...) L_ERROR("GL: " msg, ##__VA_ARGS__)


void checkGLError() {
    GLenum err;
    while ((err = glGetError()) != GL_NO_ERROR) {
        GL_ERROR("%i", err);
    }
}

void glColor32(GLcolor32 color) {
    glColor4f((float) ((color >> 0x18u) & 0xffu) / 255.0f,
              (float) ((color >> 0x10u) & 0xffu) / 255.0f,
              (float) ((color >> 0x08u) & 0xffu) / 255.0f,
              (float) ((color >> 0x00u) & 0xffu) / 255.0f);
}

void glDrawCircle(GLvec2f pos, float radius) {
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(pos.x, pos.y, 0);
    int circle_points = 100;
    float angle = 2.0f * 3.1416f / (float) circle_points;

    glDisable(GL_TEXTURE_2D);
    glBegin(GL_POLYGON);
    double angle1 = 0.0;
    glVertex2d(radius * cos(0.0), radius * sin(0.0));
    for (int i = 0; i < circle_points; i++) {
        glVertex2d(radius * cos(angle1), radius * sin(angle1));
        angle1 += angle;
    }
    glEnd();
    glPopMatrix();
}
void glOutlineRectf(GLvec2f size) {
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_LINE_LOOP);
    glVertex2f(     0,      0);
    glVertex2f(     0, size.y);
    glVertex2f(size.x, size.y);
    glVertex2f(size.x,      0);
    glEnd();
}

// endregion






// region GLL

#define GLL_ERROR(msg, ...) L_ERROR("GLL: " msg, ##__VA_ARGS__)

typedef enum {
    GLL_TYPE_Root,
    GLL_TYPE_Object,
    GLL_TYPE_Linear
} GLLType;

typedef struct GLLayout GLLayout;

struct GLLayout {
    GLLayout *parent;
    GLLType type;
    GLvec2f size;
    bool drawDebug;
    void (*add)(GLLayout *this, GLvec2f v);
};


typedef struct {
    GLLayout base;
} GLLRoot;

typedef struct {
    GLLayout base;
} GLLObject;

typedef struct {
    GLLayout base;
    bool isHorizontal;
} GLLLinear;

static GLLayout *gll_head__ = NULL;
static bool gll_doDraw__ = true;
static GLFont *gll_font__ = NULL;
static GLcolor32 gll_debugColor__ = false;


bool glLIsDrawing(void) {
    return gll_doDraw__;
}
GLFont *glLGetFont(void) {
    return gll_font__;
}
void glLSetFont(GLFont *font) {
    gll_font__ = font;
}
GLcolor32 glLGetDebug(void) {
    return gll_debugColor__;
}
void glLSetDebug(GLcolor32 debug) {
    gll_debugColor__ = debug;
}


static void glLMinSizeInternal(GLLayout *this, GLvec2f v) {
    if (v.x > this->size.x) this->size.x = v.x;
    if (v.y > this->size.y) this->size.y = v.y;
}

static void GLLRoot_add(GLLayout *this, GLvec2f v) {
    glLMinSizeInternal(this, v);
}
static void GLLObject_add(GLLayout *this, GLvec2f v) {
    glLMinSizeInternal(this, v);
}
static void GLLLinear_add(GLLayout *this, GLvec2f v) {
    GLLLinear *layout = (GLLLinear *) this;
    if (layout->isHorizontal) {
        if (v.y > this->size.y) this->size.y = v.y;
        this->size.x += v.x;
        glLTranslatef(v.x, 0);
    } else {
        if (v.x > this->size.x) this->size.x = v.x;
        this->size.y += v.y;
        glLTranslatef(0, v.y);
    }
}

static bool glLCheckEnd(bool allowRoot, const char *where) {
    if (!gll_head__ || (!allowRoot && gll_head__->type == GLL_TYPE_Root)) {
        GLL_ERROR("Too many ends in %s", where);
        return false;
    }
    return true;
}

static GLLayout *glLGetLayout(GLLType type) {
    if (gll_head__->type != type) GLL_ERROR("Invalid layout type");
    return gll_head__;
}
static void glLBegin(size_t size, GLLType type, void (*add)(GLLayout *, GLvec2f)) {
    GLLayout *parent = gll_head__;
    gll_head__ = malloc(size);
    gll_head__->parent = parent;
    gll_head__->type = type;
    gll_head__->size = GLvec2f_ZERO;
    gll_head__->add = add;
}
static void drawDebugRect(GLvec2f size) {
    glLBeginOnDraw;
    glPushMatrix();
    glColor32(gll_debugColor__);
    glLineWidth(0.5f);
    glOutlineRectf(size);
    glPopMatrix();
    glLEndOnDraw;
}
static GLvec2f glLEnd() {
    if (!glLCheckEnd(true, __func__)) return GLvec2f_ZERO;
    GLLayout *parent = gll_head__->parent;
    GLvec2f size = gll_head__->size;
    if (gll_debugColor__) drawDebugRect(size);
    if (parent) parent->add(parent, size);
    free(gll_head__);
    gll_head__ = parent;
    return size;
}



void glLBeginRoot(bool doDraw) {
    if (gll_head__) {
        GLL_ERROR("Already started");
        return;
    }
    gll_doDraw__ = doDraw;
    glLBegin(sizeof(GLLRoot), GLL_TYPE_Root, GLLRoot_add);
    glLPush;
}
GLvec2f glLEndRoot(void) {
    if (!glLCheckEnd(true, __func__)) return GLvec2f_ZERO;
    glLPop;
    GLvec2f size = glLEnd();
    size.x -= 0.001f;
    size.y -= 0.0f;
    gll_doDraw__ = true;
    return size;
}

void glLBeginDraw(void) {
    glLBeginRoot(true);
}
GLvec2f glLEndDraw(void) {
    return glLEndRoot();
}
void glLBeginMeasure(void) {
    glLBeginRoot(false);
}
GLvec2f glLEndMeasure(void) {
    return glLEndRoot();
}


void glLBeginObject(void) {
    if (!gll_head__ || gll_head__->type == GLL_TYPE_Root) {
        GLL_ERROR("GLLObject needs a parent");
        return;
    }
    glLBegin(sizeof(GLLObject), GLL_TYPE_Object, GLLObject_add);
    glLPush;
}
void glLEndObject(void) {
    if (!glLCheckEnd(false, __func__)) return;
    glLPop;
    glLEnd();
}



static void glLBeginLinear(bool isHorizontal) {
    if (!gll_head__) {
        GLL_ERROR("GLLLinear needs a parent");
        return;
    }
    glLBegin(sizeof(GLLLinear), GLL_TYPE_Linear, GLLLinear_add);
    ((GLLLinear *) glLGetLayout(GLL_TYPE_Linear))->isHorizontal = isHorizontal;
    glLPush;
}
static void glLEndLinear(void) {
    if (!glLCheckEnd(false, __func__)) return;
    glLPop;
    glLEnd();
}

void glLBeginVertical(void) {
    glLBeginLinear(false);
}
void glLEndVertical(void) {
    if (!glLCheckEnd(false, __func__)) return;
    if (((GLLLinear *) glLGetLayout(GLL_TYPE_Linear))->isHorizontal) {
        GLL_ERROR("GLLLinear is not vertical");
        return;
    }
    glLEndLinear();
}
void glLBeginHorizontal(void) {
    glLBeginLinear(true);
}
void glLEndHorizontal(void) {
    if (!glLCheckEnd(false, __func__)) return;
    if (!((GLLLinear *) glLGetLayout(GLL_TYPE_Linear))->isHorizontal) {
        GLL_ERROR("GLLLinear is not horizontal");
        return;
    }
    glLEndLinear();
}


void glLMinSize(GLvec2f v) {
    glLMinSizeInternal(gll_head__, v);
}
void glLSpacer(GLfloat s) {
    switch (gll_head__->type) {
        case GLL_TYPE_Root:
        case GLL_TYPE_Object:
            glLSimpleObject((void) 0, ((GLvec2f) {s, s}));
            break;
        case GLL_TYPE_Linear: {
            GLvec2f size = ((GLLLinear *) glLGetLayout(GLL_TYPE_Linear))->isHorizontal
                           ? (GLvec2f) {s, 0}
                           : (GLvec2f) {0, s};
            glLSimpleObject((void) 0, size);
            break;
        }
    }
}


// endregion






// region Font

MAP_DEF(glyphs);



static GLFontGlyph *glFontGlyphNew(GLFont *f, wchar_t code, FT_GlyphSlot slot, bool withTexture) {
    GLFontGlyph *glyph = malloc(sizeof(GLFontGlyph));

    GLfloat w = slot->metrics.width * f->ppu.x;
    GLfloat h = slot->metrics.height * f->ppu.y;
    GLfloat hx = slot->metrics.horiBearingX * f->ppu.x;
    GLfloat hy = slot->metrics.horiBearingY * f->ppu.y;
    GLfloat ha = slot->metrics.horiAdvance * f->ppu.x;
    GLfloat vx = slot->metrics.vertBearingX * f->ppu.x;
    GLfloat vy = slot->metrics.vertBearingY * f->ppu.y;
    GLfloat va = slot->metrics.vertAdvance * f->ppu.x;

    *glyph = (GLFontGlyph) {
            .code = code,
            .texture = 0,
            .metrics = {
                    .size = {.x = w, .y = h},
                    .hori = {
                            .off = {.x = hx, .y = hy},
                            .advance = {.x = ha, .y = 0}
                    },
                    .vert = {
                            .off = {.x = vx, .y = vy},
                            .advance = {.x = va, .y = 0}
                    }
            }
    };

    if (!withTexture) return glyph;

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glGenTextures(1, &glyph->texture);
//    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, glyph->texture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA,
                 slot->bitmap.width, slot->bitmap.rows,
                 0, GL_ALPHA, GL_UNSIGNED_BYTE,
                 slot->bitmap.buffer);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);


//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    
    return glyph;
}

static void glFontGlyphFree(GLFontGlyph *glyph) {
    // TODO call in glFontClose
    if (glyph->texture) glDeleteTextures(1, &glyph->texture);
    free(glyph);
}


GLFont *glFontLoad(FT_Library ft, char *file, FT_UInt pixelSize) {
    GLFont *f = malloc(sizeof(GLFont));
    if (FT_New_Face(ft, file, 0, &f->face)) {
        free(f);
        return NULL;
    }
    f->glyphCache = map_new(glyphs, NULL, map_hash_wchar, 0xFF + 1);
    f->glyph = NULL;
    FT_Set_Pixel_Sizes(f->face, 0, pixelSize);
    f->ppu.x = (GLfloat) f->face->size->metrics.x_ppem / (GLfloat) f->face->units_per_EM;
    f->ppu.y = (GLfloat) f->face->size->metrics.y_ppem / (GLfloat) f->face->units_per_EM;
    f->color = 0x000000FF;
    f->orientation = FONT_HORIZONTAL;
    f->sizeFactor = pixelSize / 10.0f;
    return f;
}
void glFontClose(GLFont **f) {
    if (!*f) return;
    // TODO free all glyphs (glFontGlyphFree)
    map_free(glyphs, (*f)->glyphCache);
    FT_Done_Face((*f)->face);
    free(*f);
    *f = NULL;
}
void glFontSetColor(GLFont *f, GLcolor32 color) {
    if (f) f->color = color;
}
void glFontSetSizeFactor(GLFont *f, GLfloat sizeFactor) {
    if (f) f->sizeFactor = sizeFactor;
}


static GLFontGlyph *glFontLoadGlyphInternal(GLFont *f, wchar_t code, bool withTexture) {
    if (!f || !f->face) return NULL;
    if (f->glyph && f->glyph->code == code && (!withTexture || f->glyph->texture)) return f->glyph;
    if (map_contains(glyphs, f->glyphCache, code, &f->glyph)) {
        if (!withTexture || f->glyph->texture) return f->glyph;
        glFontGlyphFree(f->glyph);
    }
    if (FT_Load_Char(f->face, code, FT_LOAD_RENDER)) return NULL;
    f->glyph = glFontGlyphNew(f, code, f->face->glyph, withTexture);
    map_put(glyphs, f->glyphCache, code, f->glyph);
    return f->glyph;
}
GLFontGlyphMetrics *glFontLoadGlyphMetrics(GLFont *f, wchar_t code) {
    GLFontGlyph *glyph = glFontLoadGlyphInternal(f, code, false);
    return glyph ? &glyph->metrics : NULL;
}
GLFontGlyph *glFontLoadGlyph(GLFont *f, wchar_t code) {
    return glFontLoadGlyphInternal(f, code, true);
}
GLvec2f glFontDrawGlyph(GLFont *f, wchar_t code) {
    if (!f || !f->face) return GLvec2f_ZERO;
    GLFontGlyph *g = glFontLoadGlyphInternal(f, code, glLIsDrawing());
    FT_Face face = f->face;
    GLFontGlyphMetricsDirection dir = f->orientation == FONT_HORIZONTAL ? g->metrics.hori : g->metrics.vert;

    GLfloat baseline = (GLfloat) face->ascender * f->ppu.y;
    GLfloat sx = f->sizeFactor / (GLfloat) face->size->metrics.x_ppem;
    GLfloat sy = f->sizeFactor / (GLfloat) face->size->metrics.y_ppem;
    GLfloat w = g->metrics.size.x;
    GLfloat h = g->metrics.size.y;
    GLfloat x = dir.off.x;
    GLfloat y = dir.off.y;
    GLfloat ax = dir.advance.x;
    GLfloat ay = dir.advance.y;
    GLint u = 1;
    GLint v = 1;


//            .x = (x + w + ax) * sx,
//            .y = (baseline - y + h + ay) * sy
    GLvec2f measure = f->orientation == FONT_HORIZONTAL
            ? (GLvec2f) {.x = ax * sx, .y = h * sy}
            : (GLvec2f) {.x = w * sx, .y = ay * sy};

    glLBeginOnDraw;
            glPushMatrix();
            glTranslatef(0, baseline * sy, 0);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, g->texture);
            glMatrixMode(GL_TEXTURE);
            glLoadIdentity();
            glMatrixMode(GL_MODELVIEW);
            glColor32(f->color);
            glBegin(GL_QUADS);
            {
                glTexCoord2i(0, 0); glVertex3f((0 + x) * sx, (0 - y) * sy, 0);
                glTexCoord2i(u, 0); glVertex3f((w + x) * sx, (0 - y) * sy, 0);
                glTexCoord2i(u, v); glVertex3f((w + x) * sx, (h - y) * sy, 0);
                glTexCoord2i(0, v); glVertex3f((0 + x) * sx, (h - y) * sy, 0);
            }
            glEnd();
            glDisable(GL_TEXTURE_2D);
            glPopMatrix();
            glTranslatef(ax * sx, ay * sy, 0);
    glLEndOnDraw;

    return measure;
}
GLvec2f glFontNewLine(GLFont *f) {
    GLfloat lineDela = f->orientation == FONT_HORIZONTAL
                       ? (GLfloat) f->face->height * f->ppu.y * f->sizeFactor / (GLfloat) f->face->size->metrics.y_ppem
                       : (GLfloat) f->face->max_advance_width * f->ppu.x * f->sizeFactor / (GLfloat) f->face->size->metrics.x_ppem;
    if (f->orientation == FONT_HORIZONTAL) glLTranslatef(0, lineDela);
    else glLTranslatef(lineDela, 0);
    return f->orientation == FONT_HORIZONTAL
           ? (GLvec2f) { .x = 0, .y = lineDela }
           : (GLvec2f) { .x = lineDela, .y = 0 };
}
GLvec2f glFontDrawTextW(GLFont *f, wchar_t *text) {
    if (!f || !f->face) return GLvec2f_ZERO;
    GLvec2f measure = GLvec2f_ZERO;
    GLvec2f tmp;
    GLfloat lineSize = 0;
    GLfloat lineSizeMax = 0;
    glLPush;
    wchar_t c;
    while ((c = *text) != L'\0') {
        if (c == L'\r' || c == L'\n') {
            if (lineSize > lineSizeMax) lineSizeMax = lineSize;
            lineSize = 0;
            glLPop;
            if (c == L'\n') {
                tmp = glFontNewLine(f);
                measure.x += tmp.x;
                measure.y += tmp.y;
            }
            glLPush;
        } else {
            tmp = glFontDrawGlyph(f, *text);
            lineSize += f->orientation == FONT_HORIZONTAL ? tmp.x : tmp.y;
        }
        text++;
    }
    glLPop;
    
    if (lineSize > lineSizeMax) lineSizeMax = lineSize;
    glLPush;
    tmp = glFontNewLine(f);
    glLPop;
    measure.x += tmp.x;
    measure.y += tmp.y;
    
    if (f->orientation == FONT_HORIZONTAL) glLTranslatef(lineSize, 0);
    else glLTranslatef(0, lineSize);
    if (f->orientation == FONT_HORIZONTAL) measure.x = lineSizeMax;
    else measure.y = lineSizeMax;
    return measure;
}
GLvec2f glFontDrawTextF(GLFont *f, const char *format, ...) {
    if (!f || !f->face) return GLvec2f_ZERO;
    va_list args1;
    va_list args2;
    va_start(args1, format);
    va_copy(args2, args1);
    size_t needed = vsnprintf(NULL, 0, format, args1) + 1;
    char *s = malloc(needed * sizeof(wchar_t) / sizeof(char));
    vsprintf(s, format, args2);
    wchar_t *ws = (wchar_t *) s;
    for (size_t i = needed; i--;) ws[i] = (unsigned char) s[i];
    GLvec2f measure = glFontDrawTextW(f, ws);
    free(s);
    va_end(args2);
    va_end(args1);
    return measure;
}

// endregion