#include "memvis_gl.h"

#include <stdio.h>

#include "../common/log.h"


static size_t getRows(memvis_gl_t *m) {
    return (m->mem_size + m->cols - 1) / m->cols;
}

GLvec2f memvis_gl_getSize(memvis_gl_t *m) {
    return (GLvec2f) {
        m->cell.width + m->cell.width * m->cols + m->cell.width,
        m->cell.height + m->cell.height * getRows(m) + m->cell.height
    };
}

void memvis_gl_init(memvis_gl_t *m) {
    m->bufferSize = sizeof(GLfloat3) * m->mem_size * 4;
    m->tmp = malloc(m->bufferSize);

    float x, y = 0;
    size_t i = 0;
    while (i < m->mem_size) {
        x = 0;
        for (size_t c = 0; c < m->cols && i < m->mem_size; c++) {
            m->tmp[i * 4 + 0] = (GLfloat3) {{x + 0, y + 0, 0}};
            m->tmp[i * 4 + 1] = (GLfloat3) {{x + m->cell.width, y + 0, 0}};
            m->tmp[i * 4 + 2] = (GLfloat3) {{x + m->cell.width, y + m->cell.height, 0}};
            m->tmp[i * 4 + 3] = (GLfloat3) {{x + 0, y + m->cell.height, 0}};

            i++;
            
            x += m->cell.width;
        }
        y += m->cell.height;
    }
    
    
    glGenBuffers(1, &(m->vbo[0]));
    glBindBuffer(GL_ARRAY_BUFFER, m->vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, m->bufferSize, m->tmp, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    for (i = 0; i < m->bufferSize / sizeof(GLfloat3); i++) {
        GLfloat3 color = {{0.0f, 0.0f, 0.0f}};
        m->tmp[i] = color;
    }

    glGenBuffers(1, &m->vbo[1]);
    glBindBuffer(GL_ARRAY_BUFFER, m->vbo[1]);
    glBufferData(GL_ARRAY_BUFFER, m->bufferSize, m->tmp, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
}

void memvis_gl_free(memvis_gl_t *m) {
    free(m->tmp);
}

static void memvis_gl_drawMarkings(memvis_gl_t *m) {
    glPushMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_TEXTURE_2D);
    
    size_t xDelta = 0x10;
    float markerFactor = (float) xDelta / 2;

    glColor32(0xFFFFFFFF);
    
    for (size_t c = m->cols / xDelta; c; c--) {
        glBegin(GL_QUADS);
        glVertex3f(                           0,              0, 0);
        glVertex3f(m->cell.width * markerFactor,              0, 0);
        glVertex3f(m->cell.width * markerFactor, m->cell.height, 0);
        glVertex3f(                           0, m->cell.height, 0);
        glEnd();

        glTranslatef(m->cell.width * xDelta, 0, 0);
    }
    glTranslatef(0, m->cell.height, 0);

    for (size_t c = getRows(m) / xDelta; c; c--) {
        glBegin(GL_QUADS);
        glVertex3f(            0,                             0, 0);
        glVertex3f(m->cell.width,                             0, 0);
        glVertex3f(m->cell.width, m->cell.height * markerFactor, 0);
        glVertex3f(            0, m->cell.height * markerFactor, 0);
        glEnd();
        glTranslatef(0, m->cell.height * xDelta, 0);
    }


    glPopMatrix();
}
void memvis_gl_draw(memvis_gl_t *m) {
    glPushMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_TEXTURE_2D);
    
    glTranslatef(0, m->cell.height, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, m->vbo[1]);
    glBufferData(GL_ARRAY_BUFFER, m->bufferSize, NULL, GL_DYNAMIC_DRAW);
    GLfloat3 *tmp = (GLfloat3 *) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    if (tmp) {
        for (size_t i = 0; i < m->mem_size; i++) {
            GLcolor32 color32u = m->colors[m->mem[i]];
            GLfloat3 color = {{(float) ((color32u >> 0x18u) & 0xffu) / 255.0f,
                                      (float) ((color32u >> 0x10u) & 0xffu) / 255.0f,
                                      (float) ((color32u >> 0x08u) & 0xffu) / 255.0f}};
            tmp[i * 4 + 0] = color;
            tmp[i * 4 + 1] = color;
            tmp[i * 4 + 2] = color;
            tmp[i * 4 + 3] = color;
        }
        glUnmapBuffer(GL_ARRAY_BUFFER);
    } else {
        L_ERROR("memvis: Unable to update color buffer");
        checkGLError();
    }
    
    
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);

    glBindBuffer(GL_ARRAY_BUFFER, m->vbo[0]);
    glVertexPointer(3, GL_FLOAT, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, m->vbo[1]);
    glColorPointer(3, GL_FLOAT, 0, 0);

    glDrawArrays(GL_QUADS, 0, m->bufferSize / sizeof(GLfloat3));

    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

    
    glPopMatrix();
    
    memvis_gl_drawMarkings(m);
}
static uint8_t linearSweep(uint8_t v, uint8_t p0, uint8_t p1, uint8_t p2, uint8_t p3) {
    if (v < p0 || p3 < v) return 0x00;
    if (v < p1) return (uint8_t) (((float) (v - p0) / (float) (p1 - p0)) * 0xFF);
    if (p2 < v) return (uint8_t) (((float) (v - p3) / (float) (p2 - p3)) * 0xFF);
    return 0xFF;
}
void memvis_gl_genColors(memvis_gl_colors_t *result, memvis_gl_colors_style_t style) {
    switch (style) {
        case MVGL_C_GRAD_BW:
            for (size_t i = 0; i < 0xFF + 1; i++) {
                (*result)[i] = 0xFF
                               | i << 8 * 1
                               | i << 8 * 2
                               | i << 8 * 3;
            }
            break;
        case MVGL_C_GRAD_SPECTRUM:
            (*result)[0] = 0x000000FF;
            for (size_t i = 1; i <= 0xFF; i++) {
                (*result)[0xFF - i + 1] = 0xFF
                                        | (GLcolor32) linearSweep((uint8_t) i, 0x80, 0xC0, 0xFF, 0xFF) << 8 * 1
                                        | (GLcolor32) linearSweep((uint8_t) i, 0x00, 0x40, 0xC0, 0xFF) << 8 * 2
                                        | (GLcolor32) linearSweep((uint8_t) i, 0x00, 0x00, 0x40, 0x80) << 8 * 3;
            }
            break;
        default:
            break;
    }
}
